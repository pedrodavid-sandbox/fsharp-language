module Tests

open Language
open System
open Xunit

[<Fact>]
let ``My test`` () =
    Assert.True(true)

[<Fact>]
let ``Can sum 2 + 2`` () =
    Assert.Equal(2 + 2, valueOf("2 + 2"))

[<Theory>]
[<InlineData(2, 3)>]
[<InlineData(3, 3)>]
let ``Can deduce value of basic sum`` (lhs, rhs) =
    Assert.Equal(lhs + rhs, valueOf(sprintf "%d + %d" lhs rhs))