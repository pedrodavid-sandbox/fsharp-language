﻿// Learn more about F# at http://fsharp.org

module Language

open System

let tryParseNumber (numberStr: string) =
    numberStr.ToCharArray()
    4

let valueOf str =
    if (String.IsNullOrEmpty(str)) then
      -1
    else if (Char.IsNumber(str.[0])) then
      tryParseNumber(str)
    else
      0



[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"
    0 // return an integer exit code
